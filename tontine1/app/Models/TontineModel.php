<?php
namespace App\Models;
use CodeIgniter\Model;
class TontineModel extends Model{
    protected $table="tontine";
    protected $allowedFields=["label", "periodicite", "dateDebut", "nbEcheance","idAdherent"];

    function tontine($idTontine){
        return $this->find($idTontine);
    }
    function listeTontineResp($idAdherent)
    {
        return $this->where('idAdherent',$idAdherent)
                    ->findAll();
    }
    // function listeTontine($idAdherent){
    //     $listPart=$this->builder('participer')
    //                     ->distinct()
    //                     ->select('idTontine')
    //                     ->where('idAdherent',$idAdherent)->get()->getResultArray();
        
    //     $idTon=[];
    //     foreach($listPart as $tp)
    //         $idTon=$tp["idTontine"];           
    //     if($idTon)
    //         $this->whereNotIn("id",$idTon);
    //     return $this->findAll();
        
    // }
    function listeTontine($idAdherent){
        
        // 1. liste des tontines dont participe l'adherent
        $listPart=$this->builder("participer")
                        ->distinct()
                        ->select('idTontine')
                        ->where('idAdherent',$idAdherent)->get()->getResultArray();
      
      //2. les idTontine de 1. dans un tableau
        $idTon=[];
        foreach ($listPart as $tp ) {
            # code...
            $idTon[]=$tp["idTontine"];
        
        }
       //3. liste des tontines dont l'adherent ne participe pas      
            if ($idTon) {
                # code...
                $this->whereNotIn("id",$idTon);            
            }
            return $this->findAll();

    }
    function mesTontine($idAdherent){
        
        // 1. liste des tontines dont participe l'adherent
        return $this->where('idAdherent',$idAdherent)->findAll();


    }
    function nbTontine(){
        return $this->selectCount("tontine.id")->get()->getResultArray();
    }
}