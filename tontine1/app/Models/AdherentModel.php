<?php
namespace App\Models;
use CodeIgniter\Model;
class AdherentModel extends Model{
    protected $table="adherent";
    protected $allowedFields=["nom", "prenom", "login", "motDePasse","profil"];
    public function participer($idTontine){
        return $this->join("participer as p","p.idAdherent=adherent.idAdherent")
                     ->join("tontine as t","t.id=p.idTontine")
                     ->where("t.id",$idTontine)
                    ->findAll();
    }
    function participant($idAdherent){
        var_dump($this->find($idAdherent));
        return $this->find($idAdherent);
    }
    public function listeParticiper(){

        return $this->where("adherent.profil","adherent")
                    ->findAll();
    }
    function cotiser($idTontine){
        $cotis=$this->selectCount('adherent.idAdherent',"nbCotis")
                ->select("adherent.idAdherent")
                ->join("cotiser c","c.idAdherent=adherent.idAdherent")
                ->join("echeance e","e.idEcheance=c.idEcheance")
                ->where("e.idTontine",$idTontine)
                ->groupBy('adherent.idAdherent')
                ->get()->getResultArray();

        $cotisations=[];
        foreach($cotis as $coti)
            $cotisations[$coti["idAdherent"]]=$coti['nbCotis'];
        return $cotisations;

    }
    function mesCotisation($idTontine){
        $idAdherent=session()->get('idAdherent');
        $cotis=$this
                ->select("adherent.idAdherent")
                ->join("cotiser c","c.idAdherent=$idAdherent")
                ->join("echeance e","e.idEcheance=c.idEcheance")
                ->where("c.idAdherent",$idAdherent)
                ->groupBy('adherent.idAdherent')
                ->get()->getResultArray();
        var_dump($cotis);

        $cotisations=[];
        foreach($cotis as $coti)
            $cotisations[$coti["idAdherent"]]=$coti['nbCotis'];
        return $cotisations;

    }
    function nbParticipant(){
        return $this->selectCount("adherent.idAdherent")
        ->where("adherent.profil","adherent")
        ->get()->getResultArray();
    }
}