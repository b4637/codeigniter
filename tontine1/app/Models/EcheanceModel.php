<?php
namespace App\Models;
use CodeIgniter\Model;
class EcheanceModel extends Model{
    protected $table="echeance";
    protected $allowedFields=["date", "numero", "idTontine"];
    function generer($tabEcheance){
        // return $this->insert(["date"=>"2022/04/11","numero"=>1,"idTontine"=>5]);
        return $this->insertBatch($tabEcheance);
    }
    function echeancesTontine($idTontine){
        return $this->where('idTontine',$idTontine)->findAll();
    }
  
}