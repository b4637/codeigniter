<?php

namespace App\Controllers;
use App\Models\AdherentModel;
helper(['html','form']);
class Utilisateur extends BaseController
{
    public function deconnexion(){
        session()->destroy();
        return redirect()->to('utilisateur/deconnexionMessage');    
    }
    public function deconnexionMessage(){
        $session=session();
        $session->setFlashData('success','deconnexion reussie');
        return redirect()->to('utilisateur');    
    }
    public function index()
    {
        $data=['titre'=>"sama tontine:: veuillez vous connecter",'menuActif'=>"connexion"];
        if($this->request->getMethod()=="post"){
            $reglesValid=[
                "login"=>["rules"=>"required","errors"=>["required"=>"le login est obligatoire"]],
                "motDePasse"=>["rules"=>"required|utilisateurValide[login,motDePasse]",
                "errors"=>["required"=>"le mot de passe est obligatoire",
                "utilisateurValide"=>"Email et/ou mot de passe incorrect(s)"]],
            ];
            if(!$this->validate($reglesValid)){
                $data['validation']=$this->validator;
            }else{
                $model= new AdherentModel();
                $user=$model->where("login",$this->request->getPost('login'))
                            ->where("motDePasse",$this->request->getPost('motDePasse'))
                            ->first();
                $dataSession=[
                    'idAdherent'=>$user["idAdherent"],
                    'nom'=>$user['nom'],
                    'prenom'=>$user['prenom'],
                    'login'=>$user['login'],
                    'profil'=>$user['profil'],
                ];
                session()->set($dataSession);
                return redirect()-> to(base_url($user["profil"]));
            }
        }
        echo view('layout/entete',$data);
        echo view('utilisateur/index');
        echo view('layout/pied');
    }

    public function inscription()
    {
        $data=['titre'=>"sama tontine::  veuillez vous inscrire",'menuActif'=>"inscription"];
        if($this->request->getMethod()=="post"){
            $reglesValid=[
                "nom"=>["rules"=>"required","errors"=>["required"=>"le nom est obligatoire"]],
                "prenom"=>["rules"=>"required","errors"=>["required"=>"le nom est obligatoire"]],
                "login"=>["rules"=>"required|min_length[6]","errors"=>["required"=>"le login est obligatoire",
                            "min_length"=>"le login doit comporter au moins six caracterers"]],
                "motDePasse"=>["rules"=>"required|min_length[6]","errors"=>["required"=>"le mot de passe est obligatoire",
                "min_length"=>"le mot de passe doit comporter au moins six caracterers"]],
                "cmotDePasse"=>["rules"=>"required|matches[motDePasse]","errors"=>["required"=>"la confirmation est obligatoire",
                            "matches"=>"la confirmation doit etre identique  au mot de passe"]],
            ];
            if(!$this->validate($reglesValid)){
            $data['validation']=$this->validator;
            }
            else{
               $adherentData=[
                   "nom"=>$this->request->getPost("nom"),
                   "prenom"=>$this->request->getPost("prenom"),
                   "login"=>$this->request->getPost("login"),
                   "motDePasse"=>$this->request->getPost("motDePasse"),
                   "profil"=>"adherent",
               ];
               $adherent= new AdherentModel();
               $adherent->insert($adherentData);
               $session=session();
               $session->setFlashdata('succces','Inscription reussie. connectez-vous');
               return redirect()->to('utilisateur');
                exit();
            }
        }

    echo view('layout/entete',$data);
    echo view('utilisateur/inscription');
    echo view('layout/pied');
}

}
