<?php

namespace App\Controllers;
helper('html');
class Home extends BaseController
{
    public function index()
    {
        $data=['titre'=>"sama tontine:: comment gerer ma tontine",'menuActif'=>"acceuil"];
        echo view('layout/entete',$data);
        echo view('welcome_message');
        echo view('layout/pied');
    }

    public function contact()
    {
        $data=['titre'=>"sama tontine:: nous contacter",'menuActif'=>"contact"];
        echo view('layout/entete',$data);
        echo view('contact');
        echo view('layout/pied');
    }

    public function presentation()
    { 
        $data=['titre'=>"sama tontine:: presentation de la societe",'menuActif'=>"presentation"];
        echo view('layout/entete',$data);;
        echo view('presentation');
        echo view('layout/pied');
    }
}
