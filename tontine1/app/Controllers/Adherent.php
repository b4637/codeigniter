<?php

namespace App\Controllers;
//1. rendre accessible la classe TontineModel
use App\Models\TontineModel;
use App\Models\AdherentModel;
use App\Models\ParticiperModel;
use App\Models\EcheanceModel;
use App\Models\CotiseModel;
use CodeIgniter\I18n\Time;
helper(['html','form']);
class Adherent extends BaseController
{
    public function payerEcheance($idAdherent,$idTontine,$idEcheance){
        //inserer la cotisation a travers le model cotise
        $modelCotise= new CotiseModel();
        $modelCotise->insert(["idAdherent"=>$idAdherent,"idEcheance"=>$idEcheance]);
        //revenir sur la page tontine avec le message de confirmation
        $session=session();
        $session->setFlashdata('successAjCotise',"cotisation enregistrées");
              return redirect()->to("adherent/participantTontine/$idTontine");
    }
    public function genererEcheance($idTontine){
        //recuperer les informations sur la tontine courante
         $model=new TontineModel();
         $maTontine=$model->tontine($idTontine);
        //creer un tableau des echeances   
        // var_dump($maTontine['periodicite']);
        $dateDebut=Time::createFromFormat('Y-m-d',$maTontine["dateDebut"]);
        $tabEcheance=[];
        for($i=1;$i<=$maTontine['nbEcheance'];$i++){
            $tabEcheance[]=['date'=>$dateDebut->toDateString(),'numero'=>$i,'idTontine'=>$idTontine];
            if($maTontine['periodicite']=='Mensuelle'){
                $dateDebut=$dateDebut->addMonths(1);
            }
            else{
                $dateDebut=$dateDebut->addDays(7);
            }
        }
        // //inserer le tableau dans la base de donnée
         $modelEcheance=new EcheanceModel();
         $nbInserer=$modelEcheance->generer($tabEcheance);
        // //revenir sur la page tontine avec une message de confirmation
          $session=session();
          $session->setFlashdata('successAjEcheance', $nbInserer."echeances ajouter");
                return redirect()->to("adherent/participantTontine/$idTontine");
    }
    public function adhererTontine($idTontine){
        $data=['titre'=>"sama tontine:: comment gerer ma tontine",'menuActif'=>"adhesion"];
        if($this->request->getMethod()=="post")
        {
            $reglesValid=[
                "montant"=>["rules"=>"required|integer",
                        "errors"=>["required"=>"Le montant est obligatoire",
                                    "integer"=>"le monatnt doit etre un nombre"]],
            ];
            if(!$this->validate($reglesValid))
            {
                $data["validation"]=$this->validator;
            }
            else
            {
                $participeData=[
                    "idTontine"=>$idTontine,
                    "montant"=>$this->request->getPost('montant'),
                    "idAdherent"=>session()->get('idAdherent'),
                ];
                $participer=new ParticiperModel();
                $participer->insert($participeData);
                $session=session();
                $session->setFlashdata('successAjAdhesion',"Adhesion avec succès");
                return redirect()->to('adherent/adhesion');
            }
        }
        else{
            $data["idTontine"]=$idTontine;
        }
        echo view('layout/entete',$data);
        echo view('adherent/ajoutAdhesion');
        echo view('layout/pied');

    }
    public function adhesion(){
        
        $data=['titre'=>"sama tontine:: comment gerer ma tontine",'menuActif'=>"adhesion"];
        $model= new TontineModel();
        $idAdherent=session()->get("idAdherent");
        $listeTontines=$model->listeTontine($idAdherent);
        $data["listeTontines"]=$listeTontines;
        echo view('layout/entete',$data);
        echo view('adherent/adhesion');
        echo view('layout/pied');

    } 
    public function participantTontine($idTontine){
        $data=['titre'=>"sama tontine:: comment gerer ma tontine",'menuActif'=>"adherentAcc"];
        //recuperer les information de la tontine courente
        $tontine=new TontineModel();
        $maTontine=$tontine->tontine($idTontine);
        //ajouter les donnee a la liste transmise
        $data["maTontine"]=$maTontine;
        //recupere liste participant
        $modelAd=new AdherentModel();
        $participants=$modelAd->participer($idTontine);
        //ajouter les donnee a la liste transmise
        $data["participants"]=$participants;
        //recuperer liste echeances
        $modelEcheance = new EcheanceModel();
        $echeances=$modelEcheance->echeancesTontine($idTontine);
        //ajouter les echeances a la liste transmise
        $data["echeances"]=$echeances;
        //recupere nombre de cotisation par adherent
        $cotisations=$modelAd->cotiser($idTontine);
        $data["cotisations"]=$cotisations;
        echo view('layout/entete',$data);
        echo view('adherent/tontine');
        echo view('layout/pied');
    }
    public function supprimerTontine($idTontine){
        $data=['titre'=>"sama tontine:: comment gerer ma tontine",'menuActif'=>"adherentAcc"];
            $tontine=new TontineModel();
           $tontine->where('id',$idTontine)->delete();
            $session=session();
            $session->setFlashdata('successAjoutTontine','suppression reussie');
            return redirect()->to( base_url().'/adherent');           
    }
    public function modifierTontine($idTontine){
        $data=['titre'=>"sama tontine:: comment gerer ma tontine",'menuActif'=>"adherentAcc"];
        $data["periodicite"]=["Mensuelle"=>"Mensuelle","Hebdomadaire"=>"Hebdomadaire"];
        $data["nbEcheance"]=[1=>1,2,3,4,5,6,7,8,9,10,11,12];
        $tontine=new TontineModel();
        if($this->request->getMethod()=="post")
        {
            $reglesValid=[
                
                "label"=>["rules"=>"required",
                        "errors"=>["required"=>"Le label est obligatoire"]],
            "periodicite"=>["rules"=>"required",
                        "errors"=>["required"=>"La Périodicité est obligatoire"]],
                        "dateDebut"=>["rules"=>"required|valid_date[d/m/Y]",
                        "errors"=>["required"=>"La date de début est obligatoire",
                        "valid_date"=>"Date non valide"]],
            "nbEcheance"=>["rules"=>"required",
                        "errors"=>["required"=>"Le nb échéance est obligatoire"]]
            ];
            if(!$this->validate($reglesValid))
            {
                $data["validation"]=$this->validator;
            }
            else
            {
                $dateDebut=Time::createFromFormat('d/m/Y',$this->request->getPost('dateDebut'));

                $tontineData=[
                    "label"=>$this->request->getPost('label'),
                    "periodicite"=>$this->request->getPost('periodicite'),
                    "dateDebut"=> $dateDebut->format("Y/m/d"),
                    "nbEcheance"=>$this->request->getPost('nbEcheance'),
                    "idAdherent"=>session()->get('idAdherent'),
                ];

                $tontine->update($idTontine,$tontineData);
                $session=session();
                $session->setFlashdata('successAjTontine',"Tontine modifié  avec succès");
                return redirect()->to('adherent');
            }
        }
        else{
            $maTontine=$tontine->tontine($idTontine);
            $dateDebut=Time::createFromFormat('Y-m-d',$maTontine['dateDebut']);
            $maTontine["dateDebut"]=$dateDebut->format("d/m/Y");
             $data["tontine"]=$maTontine;
        }
        echo view('layout/entete',$data);
        echo view('adherent/modificationTontine');
        echo view('layout/pied');
    }
    public function ajouterTontine()
    {
        $data=['titre'=>"sama tontine:: comment gerer ma tontine",'menuActif'=>"adherentAcc"];
        $data["periodicite"]=["Mensuelle"=>"Mensuelle","Hebdomadaire"=>"Hebdomadaire"];
        $data["nbEcheance"]=[1=>1,2,3,4,5,6,7,8,9,10,11,12];
        if($this->request->getMethod()=="post")
        {
            $reglesValid=[
                "label"=>["rules"=>"required",
                        "errors"=>["required"=>"Le label est obligatoire"]],
            "periodicite"=>["rules"=>"required",
                        "errors"=>["required"=>"La Périodicité est obligatoire"]],
                        "dateDebut"=>["rules"=>"required|valid_date[d/m/Y]",
                        "errors"=>["required"=>"La date de début est obligatoire",
                        "valid_date"=>"Date non valide"]],
            "nbEcheance"=>["rules"=>"required",
                        "errors"=>["required"=>"Le nb échéance est obligatoire"]]
            ];
            if(!$this->validate($reglesValid))
            {
                $data["validation"]=$this->validator;
            }
            else
            {
                $dateDebut=Time::createFromFormat('d/m/Y',$this->request->getPost('dateDebut'));
                $tontineData=[
                    "label"=>$this->request->getPost('label'),
                    "periodicite"=>$this->request->getPost('periodicite'),
                    "dateDebut"=> $dateDebut->format("Y/m/d"),
                    "nbEcheance"=>$this->request->getPost('nbEcheance'),
                    "idAdherent"=>session()->get('idAdherent'),
                ];
                $tontine=new TontineModel();
                $tontine->insert($tontineData);
                $session=session();
                $session->setFlashdata('successAjTontine',"Tontine ajouté avec succès");
                return redirect()->to('adherent');
            }
        }
        echo view('layout/entete',$data);
        echo view('adherent/ajoutTontine');
        echo view('layout/pied');
    }
    public function index()
    {
        $data=['titre'=>"sama tontine:: comment gerer ma tontine",'menuActif'=>"adherentAcc"];
       //2. instancier le modele tontine
          $model = new TontineModel();
       //3. récuperer l'id de l'adherent qui s'est connecé
           $idAdherent=session()->get('idAdherent');
       //4. recuperer la liste des tontines à partir du model
       $listeTontineResp=$model->listeTontineResp($idAdherent);
        // 5. Ajouter cette liste aux données transmises à la vue

        // TP 

        
        //recuperer les  tontine de l'utilisateur courant courente
        $mesTontines=$model->mesTontine(1);
       // var_dump( $mesTontines);
        $data["mesTontines"]=$mesTontines;

        // 
        $idTontine=9;
        // tableau
        // for($i=0;$i<=$mesTontines)
                $modelEcheance = new EcheanceModel();
                $echeances=$modelEcheance->echeancesTontine($idTontine);
                //var_dump( $echeances);
                //ajouter les echeances a la liste transmise
                $data["echeances"]=$echeances;
                //recupere nombre de cotisation par adherent
                $modelAd=new AdherentModel();
                $cotisations=$modelAd->mesCotisation($idTontine);
                $data["cotisations"]=$cotisations;
        
         $data['listeTontineResp']=$listeTontineResp;
        echo view('layout/entete',$data);
        echo view('adherent/index');
        echo view('layout/pied');
    }
}
  