<?php

namespace App\Controllers;
helper(['html','form']);
use App\Models\AdherentModel;
use App\Models\TontineModel;
class Administrateur extends BaseController
{
    public function index()
    {
        $data=['titre'=>"sama tontine:: comment gerer ma tontine",'menuActif'=>"adminAcc"];
        $modelPar= new AdherentModel();
        $nbParticipant=$modelPar->nbParticipant();
        $data["nbParticipant"]=$nbParticipant;
        $modelTon= new TontineModel();
        $nbTontine=$modelTon->nbTontine();
        $data["nbTontine"]=$nbTontine;
        echo view('layout/entete',$data);
        echo view('administrateur/index');
        echo view('layout/pied');
    }
    public function gestion(){
        $data=['titre'=>"sama tontine:: gestion utilisateur ",'menuActif'=>"gestion"];
        //recupere liste participant
        $modelAd=new AdherentModel();
        $participants=$modelAd->listeParticiper();
        //ajouter les donnee a la liste transmise
        $data["participants"]=$participants;
        echo view('layout/entete',$data);
        echo view('administrateur/gestion');
        echo view('layout/pied');
    }
    public function mdp($idAdherent){
        $modelAd=new AdherentModel();      
         $modelAd->update($idAdherent,['motDePasse'=>'tontine']);
            $session=session();
            $session->setFlashdata('success',"mot de passe modifié  avec succès");
            return redirect()->to('administrateur/gestion');
    }
}
