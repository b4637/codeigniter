<?php
namespace App\Filters;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface; 
class MembreFilter implements FilterInterface{
    public function before(RequestInterface $request,$arguments=null){
        if(!session()->get('nom')){
        session()->setFlashData('nonAutorise',"acces non autorise");
            return redirect()->to('/utilisateur');
        }
    }
    public function after(RequestInterface $request,ResponseInterface $response,$arguments=null){

    }
} 