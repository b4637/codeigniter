<?php // si au moins une tontine est gérée
          if($maTontine): ?>
         
 
<h1>Detail tontine <?= $maTontine['label'] ?></h1>
<a class="btn btn-success" href="<?= base_url().'/adherent'?>" >revenir a la liste</a>
<hr>
<div class="card md-3">
    <div class="card-header">Description <?= $maTontine["label"] ?></div>
    <div class="card-body">
    <p class="card-title">Debut: <?= date_format(date_create($maTontine["dateDebut"]),"d-m-Y") ?></p>
    <p>
        Nombre echeance prevues:  <?= $maTontine["nbEcheance"] ?> echeances
        <?php if(!$echeances): ?>              
        <a href="<?= base_url()?>/adherent/genererEcheance/<?= $maTontine['id']?>" class="btn btn-success">generer</a>
        <?php endif;?>
    </p>
    <p>
        <?php foreach($echeances as $echeance): ?>
            <span class="badge rounded-pill bg-primary">
            <?= date_format(date_create($echeance["date"]),"d/m/Y") ?>
            </span>
        <?php endforeach;?>
    </p>
    <?php if(session()->get("successAjEcheance")): ?>
        <div class="row alert alert-success">
          <?= session()->get("successAjEcheance") ?>
      </div>
      <?php endif; ?> 
    </div>

</div>
<div class="card">
    <div class="card-header">Les participants</div>
    <div class="card-body">
   <?php if(!$participants):?>
    <p>Aucun participant a cette tontine</p>
    <?php else: ?>
        <ul class="list-group" >
             <?php foreach($participants as $participant): ?>
                <li class="list-group-item">
                <h5><?= $participant["prenom"]."  ".$participant["nom"] ?></h5>
                <p> cotisation :<?= $participant["montant"] ?> cfa </p>
                <?php if(session()->get("successAjCotise")): ?>
                     <div class="row alert alert-success">
                        <?= session()->get("successAjCotise") ?>
                      </div>
                <?php endif; ?> 
                <?php 
                    $j=78;
                    if(isset($cotisations[$participant["idAdherent"]])):
                        for($i=0;$i<$cotisations[$participant["idAdherent"]];$i++): 
                            // $j++;
                 ?>
                 <span class="badge rounded-pill bg-success">
                 <?= date_format(date_create($echeances[$i]["date"]),"d/m/Y") ?>
                </span>
                <?php endfor; endif; ?> 
                <p>
                    <a class="btn btn-warning" href="<?= base_url()?>/adherent/payerEcheance/<?= $participant["idAdherent"] ?>/<?= $maTontine["id"] ?>/<?= $j+1 ?> ">payer</a>
                </p>
                </li>
            <?php endforeach; ?>
        </ul>

        <?php endif; ?> 
</div>


<?php endif; ?> 




