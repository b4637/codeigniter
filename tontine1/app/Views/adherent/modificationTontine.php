<h1>Modification Tontine</h1>

     
        <form method="post">
          <?= form_hidden("id",isset($tontine["id"])?$tontine["id"]:set_value("id")) ?>
          <div class="row g-3">
            <div class="py-3 col-sm-6">
              <label for="label" class="form-label">Label</label>
              <?= form_input(['name'=>'label','class'=>"form-control",'placeholder'=>"saisir le label",'value'=>isset($tontine["label"])?$tontine["label"]:set_value("label")])?>
            </div>
            <div class="py-3 col-sm-6">
              <label for="periodicite" class="form-label">Périodicité</label>
              <?= form_dropdown('periodicite',$periodicite,isset($tontine["periodicite"])?$tontine["periodicite"]:set_value("periodicite"),['class'=>"form-control"])?>
            </div>

            <div class="py-3 col-sm-6">
              <label for="dateDebut" class="form-label">Date Début</label>
              <?= form_input(['name'=>'dateDebut','class'=>"form-control",'placeholder'=>"jj/mm/AAAA",'value'=>isset($tontine["dateDebut"])?$tontine["dateDebut"]:set_value("dateDebut")])?>
            </div>
            <div class="py-3 col-sm-6">
              <label for="nbEcheance" class="form-label">Nb échéance</label>
              <?= form_dropdown('nbEcheance',$nbEcheance,isset($tontine["nbEcheance"])?$tontine["nbEcheance"]:set_value("nbEcheance"),['class'=>"form-control"])?>
            </div>
           

         <hr class="my-4">
         <?= form_submit(['name'=>'ajouter','class'=>"w-100 btn btn-primary btn-lg", 'value'=>'Ajouter']) ?>
         </form>
        </div>
