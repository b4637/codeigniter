<h1>Adherer a une tontine</h1>
<p>Vous pouvez adherer aux tontines suivantes</p>
<h2> les tontines disponibles</h2>
<?php if(session()->get("successAjAdhesion")): ?>
        <div class="row alert alert-success">
          <?= session()->get("successAjAdhesion") ?>
      </div>
      <?php endif; ?> 

<table class="table">
    <tr><th>Label</th><th>Périodicité</th><th>Date debut</th><th>Nb échéance</th><th>Action</th></tr>
    
<?php if(!$listeTontines): ?>
    <tr><td colspan="5" class="table-danger text center">
        Aucune tontine gérée pour l'instant
</td>
</tr>
<?php // si au moins une tontine est gérée
else: foreach($listeTontines as $tontine): ?>

<tr><td><?= $tontine['label']?></td><td><?= $tontine['periodicite']?></td><td><?= $tontine['dateDebut']?></td><td><?= $tontine['nbEcheance']?></td>
    <td>
        <a href="<?= base_url()?>/adherent/adhererTontine/<?= $tontine["id"]?>" class="btn btn-success">Adherer</a>
       </td>
</tr>
<?php endforeach; ?>
<?php endif; ?>
</table>