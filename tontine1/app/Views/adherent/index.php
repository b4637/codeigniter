<h1>Bienvenue <?= session()->get('prenom').' '.session()->get('nom') ?></h1>
<p>bous pouvez gerer vos tontine, adherer aux tontines disponibles, créer de nouvelles tontine</p>
<h2> Les tontines gérées
<a href="<?= base_url()?>/adherent/ajouterTontine" class="btn btn-success"> Nouvelle Tontine </a>
</h2>
<?php if(session()->get("successAjTontine")): ?>
        <div class="row alert alert-success">
          <?= session()->get("successAjTontine") ?>
      </div>
      <?php endif; ?> 
<table class="table">
    <tr><th>Label</th><th>Périodicité</th><th>Date debut</th><th>Nb échéance</th><th>Action</th></tr>
<?php if(!$listeTontineResp): ?>
    <tr><td colspan="5" class="table-danger text center">
        Aucune tontine gérée pour l'instant
</td>
</tr>
<?php // si au moins une tontine est gérée
else: foreach($listeTontineResp as $tontine): ?>

<tr><td><?= $tontine['label']?></td><td><?= $tontine['periodicite']?></td><td><?= $tontine['dateDebut']?></td><td><?= $tontine['nbEcheance']?></td>
    <td>
        <a href="<?= base_url()?>/adherent/modifierTontine/<?= $tontine["id"]?>" class="btn btn-warning">Modifier</a>
        <a onclick="return confirm('voulez vous supprimer la tontine <?= $tontine['label']?> ')" href="<?= base_url()?>/adherent/supprimerTontine/<?= $tontine["id"]?>" class="btn btn-danger">Supprimer</a>
        <a href="<?= base_url()?>/adherent/participantTontine/<?= $tontine["id"]?>" class="btn btn-info">Participant</a>
    </td>
</tr>
<?php endforeach; ?>
<?php endif; ?>
</table>


<!-- mes Tontines -->
<h2> Mes Tontines</h2>
<div class="card">
    <div class="card-body">
   <?php if(!$mesTontines):?>
    <p>Aucune Tontine participé</p>
    <?php else: ?>
        <ul class="list-group" >
             <?php foreach($mesTontines as $mesTontine): ?>
              <div class="card">
              <div class="card-header">Tontine : <?= $mesTontine["label"] ?></div>
                 <div class="card-body">
                    <p class="text-center"> Cotisation  <?= $mesTontine["periodicite"] ?> </p>
                    
                           
                         <?php      
                                $j=78;
                                if(isset($cotisations['idAdherent'])):
                                    for($i=0;$i<$cotisations["idAdherent"];$i++): 
                                        // $j++;
                            ?>
                            <span class="badge rounded-pill bg-success">
                            <?= date_format(date_create($echeances[$i]["date"]),"d/m/Y") ?>
                            </span>
                        <?php endfor; endif; ?> 
                   
                 </div>
              </div>
            
            
            <?php endforeach; ?>
        </ul>

     <?php endif; ?> 
</div>