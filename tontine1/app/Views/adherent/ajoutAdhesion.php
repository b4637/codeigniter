<h1>Adherer a une tontine</h1>
      <?php if(isset($validation)): ?>
        <div class="row alert alert-danger">
          <?= $validation->listErrors(); ?>
      </div>
      <?php endif; ?>
        <form method="post" >
          <?= form_hidden('idTontine',isset($idTontine)?$idTontine:set_value("idTontine"));
           ?>
          <div class="row g-3">
            <div class="py-3 col-sm-12">
              <label for="label" class="form-label">Montant</label>
              <?= form_input(['name'=>'montant','class'=>"form-control",'placeholder'=>"saisir le montant",'value'=>set_value("montant")])?>
            </div>            

         <hr class="my-4">
         <?= form_submit(['name'=>'adherer','class'=>"w-100 btn btn-primary btn-lg", 'value'=>'Adherer']) ?>
         </form>
        </div>
    