<h1>Inscription</h1>
      <?php if(isset($validation)): ?>
        <div class="row alert alert-danger">
          <?= $validation->listErrors(); ?>
      </div>
      <?php endif; ?>
        <form method="post" class="needs-validation" novalidate>
          <div class="row g-3">
            <div class="py-3 col-sm-6">
              <label for="label" class="form-label">Label</label>
              <?= form_input(['name'=>'label','class'=>"form-control",'placeholder'=>"saisir le label",'value'=>set_value("label")])?>
            </div>
            <div class="py-3 col-sm-6">
              <label for="periodicite" class="form-label">Périodicité</label>
              <?= form_dropdown('periodicite',$periodicite,set_value("periodicite"),['class'=>"form-control"])?>
            </div>

            <div class="py-3 col-sm-6">
              <label for="dateDebut" class="form-label">Date Début</label>
              <?= form_input(['name'=>'dateDebut','class'=>"form-control",'placeholder'=>"jj/mm/AAAA",'value'=>set_value("dateDebut")])?>
            </div>
            <div class="py-3 col-sm-6">
              <label for="nbEcheance" class="form-label">Nb échéance</label>
              <?= form_dropdown('nbEcheance',$nbEcheance,set_value("nbEcheance"),['class'=>"form-control"])?>
            </div>

            

         <hr class="my-4">
         <?= form_submit(['name'=>'ajouter','class'=>"w-100 btn btn-primary btn-lg", 'value'=>'Ajouter']) ?>
         </form>
        </div>
    