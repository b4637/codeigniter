    <h1>Inscription</h1>
      <?php if(isset($validation)): ?>
        <div class="row alert alert-danger">
          <?= $validation->listErrors(); ?>
      </div>
      <?php endif; ?>
        <form method="post" class="needs-validation" novalidate>
          <div class="row g-3">
            <div class="py-3 col-sm-6">
              <label>Prenom</label>
              <input type="text" class="form-control" name="prenom" id="prenom" placeholder="saisir le prenom" value="<?= set_value('prenom')?>" required>
              <div class="invalid-feedback">
                le prenom first est obligatoire
              </div>
            </div>

            <div class="py-3 col-sm-6">
            <label>Nom</label>
              <input type="text" class="form-control" name="nom" id="nom" placeholder="saisir le nom" value="<?= set_value('nom')?>" required>
              <div class="invalid-feedback">
                le nom  est obligatoire
              </div>
            </div>
         </div>

            <div class="py-3 col-12">
              <label>Login</label>
              <div class="input-group has-validation">
                <input type="text" class="form-control" name="login"  id="login" value="<?= set_value('login')?>" placeholder="saisir le login" required>
              <div class="invalid-feedback">
                  le login est obligatoire
                </div>
              </div>
            </div>

            <div class="row g-3">
              <div class="py-3 col-sm-6">
                <label>Mot de passe</label>
                <input type="password" class="form-control" name="motDePasse" id="motDePasse" placeholder="saisir le mot de passe" value="" required>
                <div class="invalid-feedback">
                 le mot de passe est obligatoire
                </div>
              </div>

              <div class="py-3 col-sm-6">
               <label>Confirmation mot du passe</label>
               <input type="password" class="form-control" name="cmotDePasse" id="cmotDePasse" placeholder="confirmer le mot de passe" value="" required>
                <div class="invalid-feedback">
                  la confirmation est obligatoire
                </div>
              </div>
            </div>
            
         <hr class="my-4">
          <button class="w-100 btn btn-primary btn-lg" type="submit">S'inscrire</button>
        </form>
      </div>

<script>
  // Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict'

  // Fetch all the forms we want to apply custom Bootstrap validation styles to
  var forms = document.querySelectorAll('.needs-validation1x')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()
</script>