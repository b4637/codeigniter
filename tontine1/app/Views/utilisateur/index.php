    <h1>Connexion </h1>
    <?php if(isset($validation)): ?>
      <div class="alert alert-danger" role="alert">
        <?= $validation->listErrors() ?>
      </div>
      <?php endif ?>
    <?php if(session()->get('success')): ?>
      <div class=" alert-success alert">
        <?= session()->get('success') ?>
      </div>
      <?php endif ?>
      <?php if(session()->get('nonAutorise')): ?>
      <div class=" alert-danger alert">
        <?= session()->get('nonAutorise') ?>
      </div>
      <?php endif ?>
 
  <form method="post">
  <h1 class="h3 mb-3 fw-normal">Entrer vos login et mot de passe</h1>
    <div class="form-floating">
      <input name="login" value="<?= set_value('login') ?>" type="text" class="form-control" id="floatingInput" placeholder="login">
      <label for="floatingInput">Login</label>
    </div>
    <div class="form-floating">
      <input name="motDePasse" value="<?= set_value('motDePasse') ?>" type="password" class="form-control" id="floatingPassword" placeholder="mot de passe">
      <label for="floatingPassword">Mot de passe</label>
    </div>

    <button class="w-100 btn btn-lg btn-primary" type="submit">Se connecter</button>

  </form>

